package skyfilabs.codeinstruct.participantsdetails;

import java.util.List;

import skyfilabs.codeinstruct.coordinatorregistrationapp.DbHandler;
import skyfilabs.codeinstruct.coordinatorregistrationapp.R;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class StudentDetailsAdapter extends ArrayAdapter<Student> {
	Context context;
	public StudentDetailsAdapter(Context context, int resource,List<Student> objects) {
		super(context, resource, objects);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
	static class ViewHolder{
		TextView name,email,phone;
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.participantdetail,parent,false);
		
		holder = new ViewHolder();
		
		holder.name = (TextView)convertView.findViewById(R.id.student_name);
		holder.phone = (TextView)convertView.findViewById(R.id.student_phone);
		holder.email = (TextView)convertView.findViewById(R.id.student_email);
		convertView.setTag(holder);
		}else{
			holder =(ViewHolder) convertView.getTag();
		}
		Student student = getItem(position);
		DbHandler db = new DbHandler(context);
		boolean flag = db.getPaymentStatus(Integer.parseInt(student.getEventid()));
		if(!flag){
			holder.email.setText(student.getEmail());
		}
		else{
		holder.email.setText(student.getEmail());
		}
		holder.phone.setText(student.getPhone()+"		         Paid:"+student.getPaymentid());
		holder.name.setText(student.getName());
		return convertView;
	}

}