package skyfilabs.codeinstruct.participantsdetails;

public class Student {
	String paymentid,name,email,phone,college,year,trainerid,eventid,studentId,workshopname,venue,branch,award,departmrnt;
	public String getDepartmrnt() {
		return departmrnt;
	}
	public void setDepartmrnt(String departmrnt) {
		this.departmrnt = departmrnt;
	}

	int id;
	public String getBranch() {
		return branch;
	}
	public String getAward() {
		return award;
	}
	public void setAward(String award) {
		this.award = award;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getWorkshopname() {
		return workshopname;
	}
	public void setWorkshopname(String workshopname) {
		this.workshopname = workshopname;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTrainerid() {
		return trainerid;
	}

	public void setTrainerid(String trainerid) {
		this.trainerid = trainerid;
	}

	public String getEventid() {
		return eventid;
	}
	public void setEventid(String eventid) {
		this.eventid = eventid;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getPaymentid() {
		return paymentid;
	}
	public void setPaymentid(String paymentid) {
		this.paymentid = paymentid;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
}