package skyfilabs.codeinstruct.coordinatorregistrationapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;





import android.net.ParseException;
import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText email;
	Button gobutton;
	ArrayList<WorkshopData> data = new ArrayList<WorkshopData>();
	
	Bundle savedInstanceState ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.savedInstanceState = savedInstanceState;
		DbHandler db = new DbHandler(getApplicationContext());
		List<WorkshopData>  list= db.getProfilesCount();
		int i = list.size();
		if(i==0){
			setContentView(R.layout.activity_main);
			email = (EditText)findViewById(R.id.cooremail);
			gobutton = (Button)findViewById(R.id.gobutton);		

		}
		
		else{
			WorkshopData wd  = list.get(0);
			String email = wd.getCoordinatoremail();
			Intent intent = new Intent(getApplicationContext(),ShowWorkshopsActivity.class);
			intent.putExtra("email",email);
			startActivity(intent);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@SuppressLint("NewApi")
	public void go(View v){
		data.clear();
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email.getText().toString());
		String memail = email.getText().toString().trim().toLowerCase();
		boolean flag = matcher.matches();
		if(email.getText().toString().equals("")){
			Toast.makeText(getApplicationContext(),"please enter email id",Toast.LENGTH_SHORT).show();
		}
		else if(!flag){
			Toast.makeText(getApplicationContext(),"you don't have access to this app ",Toast.LENGTH_SHORT).show();
		}else{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			try{
			postData1(memail);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
	}


	public void postData1(String memail) throws JSONException{  
		HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://register.aerotrix.com/eventcoordinators/json");
        JSONObject object = null;
        String text = null;

		StringBuilder sb = null;

		InputStream is = null;
        try {
        	object = new JSONObject();
        	object.put("email",email.getText().toString());
        	//array.put(object);
    		httppost.setHeader("json",object.toString());
            httppost.getParams().setParameter("jsonpost",object);
 
            // Execute HTTP Post Request
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            
            // for JSON:
            if(response != null)
            {
                is = response.getEntity().getContent();
 
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                sb = new StringBuilder();
                
                sb.append(reader.readLine() + "\n");
                
                String line = null;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                text = sb.toString();
            }
            try{
  		      JSONArray jArray = new JSONArray(text);
  		      DbHandler db = new DbHandler(getApplicationContext());
  		      //db.delete();
  		      JSONObject json_data=null;
  		      for(int i=0;i<jArray.length();i++){
  		             json_data = jArray.getJSONObject(i);
  		             WorkshopData wd = new WorkshopData();
  		             if(memail.equals(json_data.getString("coordinatoremail"))){
  		             String ct_name =json_data.getString("location") + " ,"+json_data.getString("coordinatoremail");//here "Name" is the column name in database
  		             wd.setLocation(json_data.getString("location"));
		             wd.setBranch(json_data.getString("branch"));
		             wd.setEventid(json_data.getString("eventid"));
		             wd.setVenue(json_data.getString("venue"));
		             wd.setWorkshop(json_data.getString("workshop"));
		             wd.setCoordinatoremail(json_data.getString("coordinatoremail"));
		             wd.setStatus(Integer.parseInt(json_data.getString("status")));
		             wd.setPassword(json_data.getString("password"));
  		             db.addWorkshopData(wd);
  		             
  		             Intent intent = new Intent(getApplicationContext(),ShowWorkshopsActivity.class);
  		             //wd = data.get(1);
  		             intent.putExtra("email",json_data.getString("coordinatoremail"));
  		             startActivity(intent);
  		             
  		             }
  		             /*; */
  		           //  data.add(wd);
  		         }
  		      }
  		      catch(JSONException e1){
  		       Toast.makeText(getBaseContext(), "error :" +e1 ,Toast.LENGTH_LONG).show();
  		       Log.e("log_tag","error "+e1);
  		      } 
  			  catch (ParseException e1) {
  				e1.printStackTrace();
  			}
 
            //Toast.makeText(getApplicationContext(),text.toString(),Toast.LENGTH_LONG).show();
 
        }catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	 @Override 
	 protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		onCreate(savedInstanceState);
	}


}