package skyfilabs.codeinstruct.coordinatorregistrationapp;

import java.util.List;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;




public class WorkshopsListAdapter extends ArrayAdapter<WorkshopData>{
	Context context;
	public WorkshopsListAdapter(Context context, int resource,List<WorkshopData> objects) {
		super(context, resource, objects);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
	static class ViewHolder{
		TextView workshopname;
		TextView venue;
		TextView branch;
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.workshops,parent,false);
		
		holder = new ViewHolder();
		
		holder.venue = (TextView)convertView.findViewById(R.id.venuetext);
		holder.workshopname = (TextView)convertView.findViewById(R.id.workshopnametext);
		holder.branch = (TextView)convertView.findViewById(R.id.branchtext);
		
		convertView.setTag(holder);
		}else{
			holder =(ViewHolder) convertView.getTag();
		}
		WorkshopData wd  = getItem(position);
		//Toast.makeText(context,wd.getVenue()+" "+wd.getWorkshop()+" "+wd.getBranch(),Toast.LENGTH_SHORT).show();
		holder.venue.setText(wd.getVenue());
		holder.workshopname.setText(wd.getWorkshop());
		holder.branch.setText(wd.getBranch());
		return convertView;
	}
}
